// MFCLibrayStudySOAPDefineTest.cpp: 定义控制台应用程序的入口点。
//
/*
	调用自定义webservice 实现 C++ 被调用
	调用地址：http://localhost:8080/xFireWebserviceProject/services/readerService?wsdl
	F:\javaProject\webserviceProject
	使用gsoap 2.8.59 F:\C++Project\vs2017C++Project\studyMFCProject\MFCLibraryStudyDemo\C++连接webservice\gsoap_2.8.59\gsoap-2.8

	wsdl2h -o soapWsdl.h http://localhost:8080/xFireWebserviceProject/services/readerService?wsdl
	soapcpp2 -C -Iimport soapWsdl.h
	
	引入文件：
		soapH.h
		soapStub.h
		soapWsdl.h
		stdsoap2.h
		soapC.cpp
		coapClient.cpp
		stdsoap2.cpp
*/

#include "stdafx.h"
#include "readerServiceHttpBinding.nsmap"
#include "soapH.h"

int main()
{
	struct soap soap;
	soap_init(&soap);
	_ns1__testMesage  request;
	_ns1__testMesageResponse   response;
	//QQ号码  
	request.in0 = new std::string("85608547");
	int iResult = soap_call___ns1__testMesage(&soap, NULL, NULL, &request, response);
	if (iResult == SOAP_OK)
	{
		printf("返回测试结果%s", response.out);
		
	}
	else
	{
		soap_print_fault(&soap, stderr);
	}
	soap_destroy(&soap);
	soap_end(&soap);
	soap_done(&soap);
    return 0;
}

